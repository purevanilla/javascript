class Gateway {

    public name: string;
    public url: string;
    public color: string;
    public logo: string;

    constructor(name: string, url: string, color: string, logo: string) {
        this.name = name;
        this.url = url;
        this.color = color;
        this.logo = logo;
    }

}