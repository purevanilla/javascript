class SessionUsage {

    creation: string;
    uses: string;

    constructor(creation?: string, uses?: string) {
        this.creation = creation;
        this.uses = uses;
    }
}